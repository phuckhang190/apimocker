# Installation

## 1. install the apimocker npm package:

`$ npm install apimocker`

## 2. run using 

`$ ./apimocker  -c config200.json`

## 3. You can try to access it:
http://localhost:9092/security-token

or using shell:

`$ curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' 'http://localhost:9092/register-customer'`


## 4. See: https://github.com/gstroup/apimocker 
for config file format documentation

